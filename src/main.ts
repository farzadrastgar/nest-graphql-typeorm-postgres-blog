import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { Logger } from '@nestjs/common';
import { ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(configService.get<number>('APP_PORT'));
  Logger.log(
    `Server started at http://localhost:${configService.get<number>(
      'APP_PORT',
    )}`,
  );
}
bootstrap();
