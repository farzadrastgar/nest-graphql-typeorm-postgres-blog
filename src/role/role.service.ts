import { Injectable } from '@nestjs/common';
import { CreateRoleInput } from './dto/create-role.input';
import { UpdateRoleInput } from './dto/update-role.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from './entities/role.entity';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role) private roleRepository: Repository<Role>,
  ) {}

  create(createRoleInput: CreateRoleInput) {
    return 'This action adds a new role';
  }

  async findAll() {
    return await this.roleRepository.find();
  }
  async findRoleUser(id) {
    return await this.roleRepository.find({
      where: { id },
      relations: ['users'],
    });
  }

  async findOne(id: number) {
    return await this.roleRepository.findOne({
      where: { id },
      relations: ['users'],
    });
  }

  update(id: number, updateRoleInput: UpdateRoleInput) {
    return `This action updates a #${id} role`;
  }

  remove(id: number) {
    return `This action removes a #${id} role`;
  }
}
