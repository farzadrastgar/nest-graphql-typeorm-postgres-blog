import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InsertResult, Repository } from 'typeorm';
import { Post } from './entities/post.entity';
import { Tag } from 'src/tag/entities/tag.entity';
import { CreatePostInput } from './dto/create-post.input';
import { TagService } from '../tag/tag.service';
import { PhotoService } from '../photo/photo.service';
@Injectable()
export class PostService {
  constructor(
    @InjectRepository(Post)
    private postRepository: Repository<Post>,
    private tagService: TagService,
    private photoService: PhotoService,
  ) {}

  async create(createPostInput: CreatePostInput): Promise<Post> {
    const { title, content, tags, photos } = createPostInput;
    let post = new Post();
    post.title = title;
    post.content = content;
    if (tags) {
      let tagsArray = tags.map(
        async (tag) => await this.tagService.upsert({ name: tag.name }),
      );
      post.tags = await Promise.all(tagsArray);
    }

    if (photos) {
      let photosArray = photos.map(
        async (photo) => await this.photoService.upsert(photo),
      );
      post.photos = await Promise.all(photosArray);
    }

    return await post.save();
  }

  async findAll(): Promise<Post[]> {
    return await this.postRepository.find({});
  }

  async findOne(id: number): Promise<Post> {
    return await this.postRepository.findOne({
      where: {
        id,
      },
    });
  }
}
