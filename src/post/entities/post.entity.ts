import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany,
  ManyToMany,
  BaseEntity,
} from 'typeorm';
import { Field, ObjectType } from '@nestjs/graphql';

import { User } from '../../user/entities/user.entity';
import { Photo } from '../../photo/entities/photo.entity';
import { Comment } from '../../comment/entities/comment.entity';
import { Tag } from 'src/tag/entities/tag.entity';

@Entity()
@ObjectType()
export class Post extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  title: string;

  @Field()
  @Column({ type: 'text' })
  content: string;

  @Field()
  @Column({ default: 0 })
  status: number;

  @Field()
  @Column({ type: 'int', default: 0 })
  views: number;

  @Field()
  @Column({ default: false })
  is_published: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => User, (user) => user.posts)
  @JoinColumn({ name: 'user_id' })
  user: User;

  @OneToMany(() => Photo, (photo) => photo.post, { cascade: true })
  photos: Photo[];

  @OneToMany(() => Comment, (comment) => comment.post)
  comments: Comment[];

  @ManyToMany(() => Tag, (tag) => tag.posts, { cascade: true })
  tags: Tag[];
}
