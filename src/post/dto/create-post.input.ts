import { InputType, Field } from '@nestjs/graphql';
import { CreatePhotoInput } from 'src/photo/dto/create-photo.input';
import { CreateTagInput } from 'src/tag/dto/create-tag.input';
@InputType('CreatePostInput')
export class CreatePostInput {
  @Field()
  title: string;

  @Field()
  content: string;

  @Field(() => [CreateTagInput], { nullable: true })
  tags?: CreateTagInput[];

  @Field(() => [CreatePhotoInput], { nullable: true })
  photos?: CreatePhotoInput[];
}
