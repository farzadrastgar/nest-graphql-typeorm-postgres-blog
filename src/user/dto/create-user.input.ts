import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsEmail, IsAlphanumeric } from 'class-validator';

@InputType()
export class CreateUserInput {
  @Field()
  @IsAlphanumeric()
  username: string;

  @Field()
  @IsAlphanumeric()
  password: string;

  @Field()
  @IsEmail()
  email: string;

  @IsOptional()
  @Field({ nullable: true })
  first_name?: string;

  @IsOptional()
  @Field({ nullable: true })
  last_name?: string;
}
