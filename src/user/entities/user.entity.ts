import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToMany,
  JoinTable,
} from 'typeorm';

import { Field, ObjectType } from '@nestjs/graphql';

import { Post } from '../../post/entities/post.entity';
import { Role } from '../../role/entities/role.entity';
import { Comment } from '../../comment/entities/comment.entity';

@ObjectType()
@Entity()
export class User {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ nullable: false, unique: true })
  username: string;

  @Field()
  @Column({ nullable: false })
  password: string;

  @Field()
  @Column({ default: null })
  first_name: string;

  @Field()
  @Column({ default: null })
  last_name: string;

  @Field()
  @Column({ unique: true })
  email: string;

  @Column({ default: 0 })
  status: number;

  @Column({ default: false })
  is_active: boolean;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  last_login: Date;

  @Field((type) => [Post])
  @OneToMany(() => Post, (post) => post.user)
  posts: Post[];

  @OneToMany(() => Comment, (comment) => comment.user)
  comments: Comment[];

  @Field((type) => [Role])
  @ManyToMany(() => Role, (role) => role.users)
  @JoinTable({
    name: 'users_roles',
    joinColumn: { name: 'user_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'role_id', referencedColumnName: 'id' },
  })
  roles: Role[];
}
