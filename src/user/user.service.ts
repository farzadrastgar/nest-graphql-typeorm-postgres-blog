import { Injectable } from '@nestjs/common';
import { CreateUserInput } from './dto/create-user.input';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { Role } from 'src/role/entities/role.entity';
import { RoleService } from 'src/role/role.service';
import { UpdateUserInput } from './dto/update-user.input';
import { todo } from 'node:test';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    private roleService: RoleService,
  ) {}

  async create(createUserInput: CreateUserInput) {
    const { username, password, first_name, last_name, email } =
      createUserInput;

    let newUser = await this.userRepository.create({
      username,
      password, //todo hash password
      first_name,
      last_name,
      email,
    });

    return await this.userRepository.save(newUser);
  }

  async findAll() {
    return await this.userRepository.find();
  }

  async findOne(id: number) {
    return await this.userRepository.findOne({
      where: { id },
      relations: ['roles', 'posts'],
    });
  }

  async getUserRoles(id: number) {
    return (await this.findOne(id)).roles;
  }

  async getUserPosts({ id }) {
    return (await this.findOne(id)).posts;
  }

  async assignRoleToUser(updateUserInput: UpdateUserInput) {
    const { user_id, role_id } = updateUserInput;
    let user = await this.findOne(user_id);
    let role = await this.roleService.findOne(role_id);

    if (!user || !role) {
      throw new Error('user or role not found');
    }
    user.roles = [role, ...user.roles];

    return await this.userRepository.save(user);
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }
}
