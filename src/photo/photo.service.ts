import { Injectable } from '@nestjs/common';
import { CreatePhotoInput } from './dto/create-photo.input';
import { UpdatePhotoInput } from './dto/update-photo.input';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Photo } from './entities/photo.entity';

@Injectable()
export class PhotoService {
  constructor(
    @InjectRepository(Photo) private photoRepository: Repository<Photo>,
  ) {}

  async upsert({ path, format, name }: CreatePhotoInput) {
    let photo = await this.photoRepository.findOneBy({ name });
    if (!photo) {
      photo = new Photo();
      photo.name = name;
      photo.format = format;
      photo.path = path;
      await photo.save();
    }
    return photo;
  }

  findAll() {
    return `This action returns all photo`;
  }

  findOne(id: number) {
    return `This action returns a #${id} photo`;
  }

  update(id: number, updatePhotoInput: UpdatePhotoInput) {
    return `This action updates a #${id} photo`;
  }

  remove(id: number) {
    return `This action removes a #${id} photo`;
  }
}
