import { ObjectType, Field, Int } from '@nestjs/graphql';
import { Post } from 'src/post/entities/post.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
  JoinColumn,
  BaseEntity,
} from 'typeorm';

@Entity()
@ObjectType()
export class Photo extends BaseEntity {
  @Field()
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column({ unique: true })
  name: string;

  @Field()
  @Column()
  format: string;

  @Field()
  @Column()
  path: string;

  @CreateDateColumn()
  create_at: Date;

  @ManyToOne(() => Post, (post) => post.photos)
  @JoinColumn({ name: 'post_id' })
  post: Post;
}
