import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreatePhotoInput {
  @Field()
  format: string;

  @Field()
  path: string;

  @Field()
  name: string;
}
